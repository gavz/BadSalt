# BadSalt
A repo containing some stagers and setup scripts for configuring Salt Stack to be a "bad"-ass C2

<p align="center">
  <img src="https://raw.githubusercontent.com/3ndG4me/BadSalt/master/badsalt-logo.PNG"/>
 </p>

## WIP (Currently only has PoC scripts, nothing fancy, very basic...)
TODO:
- ~CentOS/RHEL Support~
- ~Kali/Debian Support~
- ~Ubuntu Support~
- ~Create Stager for Windows~
- ~Create Stager for Debian/Ubuntu/etc... Linux~
- ~Create Stager for MacOS~
- Create sample post-exploitation "modules"/"recipes" or whatever they're called
- Create a config generator in GO to make life easier? Automate all the things!
- Create a Usage Guide
